import { Component } from '@angular/core';

@Component({
  selector: 'soft-drink-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'soft-drink';
}
