import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from '@soft-drink/shared/data-feedback/ui';

const routes: Routes = [
  {
    path: '',
    // component: HomePageComponent,
    loadChildren: () =>
      import('@soft-drink/home-page/feature-shell').then(
        (m) => m.HomePageFeatureShellModule
      ),
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
