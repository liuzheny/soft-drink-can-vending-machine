module.exports = {
  projects: [
    '<rootDir>/apps/soft-drink',
    '<rootDir>/libs/home-page/data-access',
    '<rootDir>/libs/home-page/feature-shell',
    '<rootDir>/libs/home-page/ui',
    '<rootDir>/libs/shared/ngxs',
    '<rootDir>/libs/shared/data-feedback/data-access',
    '<rootDir>/libs/shared/data-feedback/ui',
    '<rootDir>/libs/shared/data-entry/data-access',
    '<rootDir>/libs/shared/data-entry/ui',
  ],
};
