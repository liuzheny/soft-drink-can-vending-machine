Angular Assessment
this is a project to implement a soft drink can vending machine, which is using Angular 10. Additionally, it uses NG-ZORRO as the UI library. Furthermore, it also uses NGXS as the state management and follows the GIT-FLOW branching model.

Install npm packages
npm install
nx serve
http://localhost:4200/
