import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HandlePurchaseSoftDrinkObject, PurchaseResponse } from '../model';

const ERROR_MESSAGE_MAPPING = {
  insufficientCans: 'Out of stock',
  insufficientMoney: 'Insufficient money',
  success: 'Thank you'
}

@Injectable({
  providedIn: 'root',
})
export class SoftDrinkService {
  constructor() {}

  handlePurchaseSoftDrink = ({
    currentCount,
    price,
    count,
    money,
  }: HandlePurchaseSoftDrinkObject): Observable<PurchaseResponse> => {
    if (!this.hasEnoughCount(currentCount, count)) {
      return of({isSuccess: false, message: ERROR_MESSAGE_MAPPING.insufficientCans})
    }
    if (this.returnChanges(count, price, money) < 0) {
      return of({isSuccess: false, message: ERROR_MESSAGE_MAPPING.insufficientMoney})
    }
    return of({isSuccess: true, message: ERROR_MESSAGE_MAPPING.success})
  };

  private hasEnoughCount = (currentCount, count) => currentCount >= count;

  private returnChanges = (count, price, money) => money - price * count;
}
