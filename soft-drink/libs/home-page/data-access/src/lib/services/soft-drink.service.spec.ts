import { TestBed } from '@angular/core/testing';

import { SoftDrinkService } from './soft-drink.service';

describe('SoftDrinkService', () => {
  let service: SoftDrinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SoftDrinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
