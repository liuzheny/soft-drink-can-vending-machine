export enum HomePagePathEnum {
  Purchase = 'purchase',
  Resupply = 'resupply',
}

export interface PurchaseSoftDrinkObject {
  count: number;
  money: number;
}

export interface HandlePurchaseSoftDrinkObject extends PurchaseSoftDrinkObject {
  price: number;
  currentCount: number;
}

export interface PurchaseResponse {
  isSuccess: boolean;
  message?: string;
}
