export enum SoftDrinkActionTypes {
  GetSoftDrinkInfo = '[Soft Drink] Get Soft Drink Info',
  PurchaseSoftDrink = '[Soft Drink] Purchase Soft Drink',
  ResupplySoftDrink = '[Soft Drink] Resupply Soft Drink',
  ResetPurchaseSoftDrinkStatus = '[Soft Drink] Reset Purchase Soft Drink Status'
}
