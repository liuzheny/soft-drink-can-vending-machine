import { PurchaseSoftDrinkObject } from '../model';
import { SoftDrinkActionTypes } from './soft-drink.types';

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace SoftDrink {
  export class GetSoftDrinkInfo {
    static readonly type = SoftDrinkActionTypes.GetSoftDrinkInfo;
    constructor() {}
  }

  export class PurchaseSoftDrink {
    static readonly type = SoftDrinkActionTypes.PurchaseSoftDrink;
    constructor(public purchaseObj: PurchaseSoftDrinkObject) {}
  }

  export class ResupplySoftDrink {
    static readonly type = SoftDrinkActionTypes.ResupplySoftDrink;
    constructor(public reSupplyCount: number) {}
  }

  export class ResetPurchaseSoftDrinkStatus {
    static readonly type = SoftDrinkActionTypes.ResetPurchaseSoftDrinkStatus;
    constructor() {}
  }
}
