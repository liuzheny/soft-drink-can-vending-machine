import { Injectable } from '@angular/core';
import {
  Action,
  Actions,
  Selector,
  State,
  StateContext,
  Store,
} from '@ngxs/store';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PurchaseResponse } from '../model';
import { SoftDrinkService } from '../services';
import { SoftDrink } from './soft-drink.action';

const DEFAULT_SOF_DRINK_PRICE = 1.5;

export interface SoftDrinkStateModel {
  softDrinkCount: number;
  price: number;
  isSuccess?: boolean;
  message?: string;
  change?: number;
}

@State<SoftDrinkStateModel>({
  name: 'softDrink',
  defaults: {
    softDrinkCount: 1,
    price: DEFAULT_SOF_DRINK_PRICE,
  },
})
@Injectable()
export class SoftDrinkState {
  constructor(
    private store: Store,
    private softDrinkService: SoftDrinkService
  ) {}

  @Selector()
  static getSoftDrinkCount(state: SoftDrinkStateModel) {
    return state.softDrinkCount;
  }

  @Selector()
  static getSoftDrinkPrice(state: SoftDrinkStateModel) {
    return state.price;
  }

  @Selector()
  static getSoftDrinkStatus(state: SoftDrinkStateModel) {
    return state.isSuccess;
  }

  @Selector()
  static getSoftDrinkMessage(state: SoftDrinkStateModel) {
    return state.message;
  }

  @Selector()
  static getSoftDrinkChange(state: SoftDrinkStateModel) {
    return state.change;
  }

  @Action(SoftDrink.PurchaseSoftDrink)
  purchaseSoftDrink(
    { getState, setState }: StateContext<SoftDrinkStateModel>,
    { purchaseObj }: SoftDrink.PurchaseSoftDrink
  ) {
    const state = getState();
    const { softDrinkCount: currentCount, price } = state;
    const { count, money } = purchaseObj;
    return this.softDrinkService
      .handlePurchaseSoftDrink({ count, money, price, currentCount })
      .pipe(
        tap((result: PurchaseResponse) => {
          const { isSuccess, message } = result;
          setState({
            ...state,
            isSuccess,
            message,
            ...(isSuccess && { softDrinkCount: currentCount - count}),
            ...(isSuccess && count && money && {change: money - count * price})
          });
        })
      );
  }

  @Action(SoftDrink.ResupplySoftDrink)
  resupplySoftDrink(
    { getState, setState }: StateContext<SoftDrinkStateModel>,
    { reSupplyCount }: SoftDrink.ResupplySoftDrink
  ) {
    const state = getState();
    const { softDrinkCount } = state;
    setState({
      ...state,
      softDrinkCount: softDrinkCount + reSupplyCount,
    });
  }

  @Action(SoftDrink.ResetPurchaseSoftDrinkStatus)
  resetSoftDrinkStatus({
    getState,
    setState,
  }: StateContext<SoftDrinkStateModel>) {
    const state = getState();
    setState({
      ...state,
      isSuccess: undefined,
      message: undefined,
    });
  }
}
