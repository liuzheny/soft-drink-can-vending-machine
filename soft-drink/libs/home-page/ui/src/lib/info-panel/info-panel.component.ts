import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Select } from '@ngxs/store';
import { SoftDrinkState } from '@soft-drink/home-page/data-access';
import { Observable } from 'rxjs';

@Component({
  selector: 'sd-info-panel',
  templateUrl: './info-panel.component.html',
  styleUrls: ['./info-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoPanelComponent implements OnInit {
  @Select(SoftDrinkState.getSoftDrinkCount) softDrinkCount$: Observable<number>;
  @Select(SoftDrinkState.getSoftDrinkPrice) softDrinkPrice$: Observable<number>;
  constructor() {}

  ngOnInit(): void {}
}
