import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { CustomValidatorService, SoftDrink } from '@soft-drink/home-page/data-access';
import {
  FormHelperService,
  MarkControlState,
} from '@soft-drink/shared/data-entry/data-access';

const RESUPPLY_FORM_CONTROL_NAMES = {
  resupplyCount: 'resupplyCount',
};

@Component({
  selector: 'sd-soft-drink-resupply',
  templateUrl: './soft-drink-resupply.component.html',
  styleUrls: ['./soft-drink-resupply.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SoftDrinkResupplyComponent implements OnInit {
  resupplyForm: FormGroup;
  isLoading: boolean;
  constructor(
    private fb: FormBuilder,
    private store: Store,
    private formHelperService: FormHelperService,
    private validatorService: CustomValidatorService,
    private router: Router
  ) {
    this.resupplyForm = this.initialForm();
  }

  ngOnInit(): void {}

  navigateToHome = () => this.router.navigate([`./home/`]);

  onSubmit = () => {
    this.isLoading = this.resupplyForm.valid;
    this.formHelperService.markFormControlsState(
      this.resupplyForm,
      MarkControlState.DIRTY
    );
    if (!this.isLoading) {
      return;
    }
    this.store.dispatch(
      new SoftDrink.ResupplySoftDrink(
        this.resupplyForm.get(RESUPPLY_FORM_CONTROL_NAMES.resupplyCount).value
      )
    );
  };

  private initialForm = () => {
    return this.fb.group({
      resupplyCount: [null, [Validators.required, this.validatorService.integer()]],
    });
  };
}
