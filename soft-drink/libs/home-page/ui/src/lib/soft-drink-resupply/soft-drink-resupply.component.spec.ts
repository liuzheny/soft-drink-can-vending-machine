import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftDrinkResupplyComponent } from './soft-drink-resupply.component';

describe('SoftDrinkResupplyComponent', () => {
  let component: SoftDrinkResupplyComponent;
  let fixture: ComponentFixture<SoftDrinkResupplyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoftDrinkResupplyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftDrinkResupplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
