import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofActionSuccessful, Select, Store } from '@ngxs/store';
import {
  FormHelperService,
  MarkControlState,
} from '@soft-drink/shared/data-entry/data-access';
import {
  CustomValidatorService,
  SoftDrink,
  SoftDrinkState,
} from '@soft-drink/home-page/data-access';
import { forkJoin, Observable } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { map } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';

const PURCHASE_FORM_CONTROL_NAMES = {
  canCount: 'canCount',
  money: 'money',
};

@UntilDestroy()
@Component({
  selector: 'sd-soft-drink-purchase',
  templateUrl: './soft-drink-purchase.component.html',
  styleUrls: ['./soft-drink-purchase.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SoftDrinkPurchaseComponent implements OnInit, OnDestroy {
  @Select(SoftDrinkState.getSoftDrinkCount) softDrinkCount$: Observable<number>;
  @Select(SoftDrinkState.getSoftDrinkPrice) softDrinkPrice$: Observable<number>;
  @Select(SoftDrinkState.getSoftDrinkChange)
  softDrinkChange$: Observable<number>;
  @Select(SoftDrinkState.getSoftDrinkStatus)
  softDrinkStatus$: Observable<boolean>;
  @Select(SoftDrinkState.getSoftDrinkMessage)
  softDrinkMessage$: Observable<string>;
  purchaseForm: FormGroup;
  isLoading: boolean;
  message: string;
  change: number;
  purchaseMap = PURCHASE_FORM_CONTROL_NAMES;

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private router: Router,
    private formHelperService: FormHelperService,
    private validatorService: CustomValidatorService,
    private notification: NzNotificationService,
    private actions$: Actions,
    private changeDetectRef: ChangeDetectorRef
  ) {
    this.purchaseForm = this.initialForm();
  }

  ngOnInit(): void {
    this.softDrinkMessage$
      .pipe(untilDestroyed(this))
      .subscribe((message: string) => {
        this.message = message;
      });
    this.softDrinkChange$
      .pipe(untilDestroyed(this))
      .subscribe((change: number) => {
        this.change = change;
      });
    this.softDrinkStatus$
      .pipe(untilDestroyed(this))
      .subscribe((status: boolean) => {
        if (status !== undefined) {
          this.notification.blank(
            '',
            `${this.message}. ${
              this.change && status ? `Here is your change ${this.change}` : ``
            }`
          );
        }
        setTimeout(() =>
          this.store.dispatch(new SoftDrink.ResetPurchaseSoftDrinkStatus())
        );
      });
  }

  ngOnDestroy() {}

  navigateToHome = () => this.router.navigate([`./home/`]);

  onSubmit = () => {
    this.isLoading = this.purchaseForm.valid;
    this.formHelperService.markFormControlsState(
      this.purchaseForm,
      MarkControlState.DIRTY
    );
    if (!this.isLoading) {
      return;
    }
    this.store.dispatch(
      new SoftDrink.PurchaseSoftDrink({
        count: this.purchaseForm.get(PURCHASE_FORM_CONTROL_NAMES.canCount)
          .value,
        money: this.purchaseForm.get(PURCHASE_FORM_CONTROL_NAMES.money).value,
      })
    );
  };

  private initialForm = () => {
    return this.fb.group({
      canCount: [null, [Validators.required, this.validatorService.integer()]],
      money: [null, [Validators.required]],
    });
  };
}
