import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoftDrinkPurchaseComponent } from './soft-drink-purchase.component';

describe('SoftDrinkPurchaseComponent', () => {
  let component: SoftDrinkPurchaseComponent;
  let fixture: ComponentFixture<SoftDrinkPurchaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoftDrinkPurchaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftDrinkPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
