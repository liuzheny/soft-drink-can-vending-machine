import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { HomePagePathEnum } from '@soft-drink/home-page/data-access';

@Component({
  selector: 'sd-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionPanelComponent implements OnInit {
  homePagePath = HomePagePathEnum;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateTo = (path: string) => this.router.navigate([`./home/${path}`]);
}
