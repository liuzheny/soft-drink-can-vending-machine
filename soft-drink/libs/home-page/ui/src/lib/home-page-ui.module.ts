import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPanelComponent } from './info-panel/info-panel.component';
import { ActionPanelComponent } from './action-panel/action-panel.component';
import { NzStatisticModule } from 'ng-zorro-antd/statistic';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { SoftDrinkPurchaseComponent } from './soft-drink-purchase/soft-drink-purchase.component';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { SoftDrinkResupplyComponent } from './soft-drink-resupply/soft-drink-resupply.component';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzFormModule } from 'ng-zorro-antd/form';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const COMPONENTS = [
  InfoPanelComponent,
  ActionPanelComponent,
  SoftDrinkPurchaseComponent,
  SoftDrinkResupplyComponent,
];

const NG_ZORRO_MODULES = [
  NzStatisticModule,
  NzGridModule,
  NzInputNumberModule,
  NzFormModule,
  NzNotificationModule
];

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NG_ZORRO_MODULES],
  declarations: [COMPONENTS],
  exports: [COMPONENTS],
})
export class HomePageUiModule {}
