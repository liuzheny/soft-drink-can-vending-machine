import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageFeatureShellRoutingModule } from './home-page-feature-shell-routing.module';
import { HomePageComponent } from './home-page/home-page.component';
import { HomePageUiModule } from '@soft-drink/home-page/ui';

@NgModule({
  imports: [CommonModule, HomePageFeatureShellRoutingModule, HomePageUiModule],
  declarations: [HomePageComponent],
})
export class HomePageFeatureShellModule {}
