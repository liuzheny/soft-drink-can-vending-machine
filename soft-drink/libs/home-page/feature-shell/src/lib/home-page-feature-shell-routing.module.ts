import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { SoftDrinkPurchaseComponent, SoftDrinkResupplyComponent } from '@soft-drink/home-page/ui';

export const userRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    component: HomePageComponent,
    children: [
      {
        path: 'purchase',
        component: SoftDrinkPurchaseComponent
      },
      {
        path: 'resupply',
        component: SoftDrinkResupplyComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule],
})
export class HomePageFeatureShellRoutingModule {}
