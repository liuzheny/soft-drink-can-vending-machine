import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MarkControlState } from '../model';

@Injectable({
  providedIn: 'root'
})
export class FormHelperService {

  constructor() { }

  markFormControlsState = (
    formGroup: FormGroup,
    controlState: MarkControlState
  ) => {
    for (const key in formGroup.controls) {
      if (formGroup.get(key)) {
        if (controlState === MarkControlState.DIRTY) {
          formGroup.get(key).markAsDirty();
          formGroup.get(key).updateValueAndValidity();
        } else if (controlState === MarkControlState.RESET) {
          formGroup.get(key).reset();
        }
      }
    }
  };
}
