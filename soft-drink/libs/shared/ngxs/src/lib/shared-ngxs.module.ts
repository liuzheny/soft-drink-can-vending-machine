import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from '@ngxs/store';
import { SoftDrinkState } from '@soft-drink/home-page/data-access';

@NgModule({
  imports: [CommonModule, NgxsModule.forRoot([SoftDrinkState])],
})
export class SharedNgxsModule {}
